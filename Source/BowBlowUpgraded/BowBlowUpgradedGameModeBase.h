// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BowBlowUpgradedGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BOWBLOWUPGRADED_API ABowBlowUpgradedGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
